---
title: "Aula 5"
author: "Prof. Dr. João B. Tolentino Jr"
output: 
  pdf_document:
    number_sections: TRUE
    fig_caption: TRUE
    toc: FALSE
  html_document:
    number_sections: TRUE
    fig_caption: TRUE
    toc: TRUE
editor_options: 
  chunk_output_type: console
---
```{r, include=FALSE}
options(scipen = 999)
options(warn=-1)

library(readxl)
library(knitr)
```

[Sumário](index.html)

#[Introdução](aula1.html)

#[Estatística Básica](aula2.html)

#[Introdução à inferência](aula3.html)

#[Produção de dados](aula4.html)

#Duas amostras

##Teste t para dados emparelhados

Para comparar as respostas aos dois tratamentos em um planejamento de dados emparelhados, determine a diferença entre as respostas em cada par.  

Em seguida, aplique o procedimento _t_ de uma amostra a essas diferenças.

###EXEMPLO *Perfumes florais e aprendizado*

Pesquisadores perguntaram se odores agradáveis teriam o efeito de melhorar o desempenho de estudantes.

Vinte e um sujeitos trabalharam em um labirinto com lápis e papel, usando uma máscara inodora ou com perfume floral, em ordem aleatória.

Há evidência de que os sujeitos resolveram o labirinto mais depressa usando a máscara com perfume?

```{r, echo=FALSE, results='asis'}
library(readxl)
fl <- read_excel("data/floral.xlsx")

library(knitr)
tab.fl <- kable(fl,
                digits=2,
                col.names=c("Sujeito","Sem perfume","Com perfume","Diferença"),
                align = c("c","c","c","c"))
                
tab.fl
```

Download em planilha do Excel:  
[floral.xlsx](data/floral.xlsx)

```{r}
library(readxl)
fl <- read_excel("data/floral.xlsx")
attach(fl)

#teste t
t.test(sem, com, paired = TRUE, alternative = "greater")
t.test(diff, alternative = "greater")
t.test(diff)

#ramo e folhas
stem(diff)
#histograma
hist(diff)
```

Os dados não apoiam a afirmativa de que perfumes florais melhorem o desempenho. 

* O IC para a diferença foi de -4,75 a +6,66 segundos

* A melhora média (amostral) foi de apenas 0,95 segundos

###EXERCÍCIO  *Linguagem de Programação*

Vinte e um programadores de computador de firmas TI em todo o país foram selecionados aleatoriamente. Pediu-se a cada um para escrever um código em C++ e em Java para uma aplicação específica. 

O tempo de execução (em segundos) de cada programa, por linguagem de computador, é apresentado abaixo.

Determine se há alguma evidência de que o tempo médio de execução seja maior para programas Java do que para programas C++.

```{r, echo=FALSE, results='asis'}
library(readxl)
pg <- read_excel("data/programa.xlsx")

library(knitr)
tab.pg <- kable(pg,
                digits=2,
                col.names=c("C++","Java"),
                align = c("c","c"))
                
tab.pg
```

Download em planilha do Excel:  
[programa.xlsx](data/programa.xlsx)

\newpage

##Teste t para duas amostras

O fato de uma diferença observada entre duas médias amostrais ser surpreendente depende da dispersão das observações, bem como das duas médias. 

Médias muito diferentes podem ocorrer apenas devido ao acaso, se as observações individuais variarem bastante. 

###EXEMPLO *Peso de Latas de Alumínio*

Latas de alumínio são feitas a partir de grandes lingotes sólidos, prensados sob rolos de alta pressão e cortados como biscoitos a partir de folhas finas. 

Uma companhia afirma que um novo processo de fabricação diminui a quantidade de alumínio necessária para se fazer uma lata e, portanto, diminui o peso.

Obtiveram-se amostras aleatórias independentes de latas de alumínio feitas pelos processos velho e novo, e o peso (em gramas) de cada uma é dado na tabela.

Há alguma evidência de que as latas de alumínio feitas pelo novo processo tenham um peso médio populacional menor?

```{r, echo=FALSE, results='asis'}
library(readxl)
lt <- read_excel("data/lata.xlsx")

library(knitr)
tab.lt <- kable(lt,
                digits=2,
                col.names=c("Processo antigo","Processo novo"),
                align = c("c","c"))
                
tab.lt
```

Download em planilha do Excel:  
[lata.xlsx](data/lata.xlsx)

Há evidência que sugere que as latas feitas pelo novo processo têm um peso médio menor

```{r}
library(readxl)
lt <- read_excel("data/lata.xlsx")
attach(lt)

t.test(antigo,novo,alternative = "greater")
t.test(antigo,novo)

```

Quão menores são as latas fabricadas pelo processo novo?

0,10 a 0,82 g

###EXERCÍCIO  *Densidade Seca de Madeira*

A madeira tem várias propriedades diferentes, por exemplo, condutividade térmica, permeabilidade ao vapor e permeabilidade ao ar. A densidade seca da madeira é, em geral, usada como medida de força de cisalhamento.

Em um recente estudo, foram comparadas as forças do compensado e da tábua de fio orientado (TFO). Obtiveram-se amostras aleatórias independentes de cada tipo de madeira e a densidade seca de cada peça (em kg/$m^3$) foi medida. 

Há evidências de que a densidade seca da madeira da tábua de fio orientado (TFO) é maior do que a do compensado?

```{r, echo=FALSE, results='asis'}
library(readxl)
md <- read_excel("data/madeira.xlsx")

library(knitr)
tab.md <- kable(md,
                digits=2,
                col.names=c("Compensado","Tábua de fio orientado (TFO)"),
                align = c("c","c"))
                
tab.md
```

Download em planilha do Excel:  
[madeira.xlsx](data/madeira.xlsx)

#[Análise de Variância](aula6.html)

#[Correlação e Regressão](aula7.html)

#[Dados categóricos](aula8.html)

#[Estatística não-paramétrica](aula9.html)


